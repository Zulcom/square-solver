<?php


namespace SfuKruto\view;


class Index
{
    public static function render($a = null, $b = null, $c = null, $result = null, $note = '')
    {
        $tpl = new Tpl;
        if ($result) {
            $tpl->setIfs(
                [
                    'root_1' => !is_null($result['x'])  ? true : false,
                    'root_2' => !is_null($result['y']) != null ? true : false,
                    'note' => $note ? true : false,
                ]
            );
            $tpl->setVars(
                [
                    'value_a' => $a,
                    'value_b' => $b,
                    'value_c' => $c,
                    'root_1' => $result['x'],
                    'root_2' => $result['y'],
                    'note' => $note,
                ]
            );
        } else {
            $tpl->setIfs(
                [
                    'root_1' => false,
                    'root_2' => false,
                    'note' => false,
                ]
            );
        }
        $tpl->setTemplate('src/view/templates/index.html');
        $tpl->compile();

        return $tpl->getCompiled();
    }
}
