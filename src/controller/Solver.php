<?php

namespace SfuKruto\controller;

use SfuKruto\model\Radical;
use SfuKruto\model\Fraction;

class Solver
{
    private $note;
    private $solution = [];

    public function getFirstRoot()
    {
        return $this->solution['x'];
    }

    public function getSecondRoot()
    {
        return $this->solution['y'];
    }
    /*
     * Возвращает количество корней
     */
    public function getAmountOfRoots()
    {
        return count($this->solution);
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param $a
     * @param $b
     * @param $c
     * @return array
     */
    public function getSolution($a, $b, $c): array
    {
        $this->solve($a, $b, $c);

        return $this->solution;
    }

    private function solve($a, $b, $c)
    {
        if ($a == 0) {
            if ($b != 0) {
                $this->note = "a = 0; уравнение вырождается в линейное:";
                $this->solution['x'] = (new Fraction(-$c, $b))->__toString();
            } elseif ($c == 0) {
                $this->note = 'Все коэффициенты равны нулю';
                $this->solution['x'] = 'Любое число';
            } else {
                $this->note = 'Нет решений';
            }
        } else {
            $discriminant = $b * $b - 4 * $a * $c;
            if ($discriminant != 0) {
                $this->note = 'Два корня';
                $dRoot = new Radical($discriminant);
                if ($dRoot->isInteger()) {
                    $this->solution['x'] = (new Fraction(
                        -$b + sqrt($discriminant), 2 * $a
                    ))->__toString();
                    $this->solution['y'] = (new Fraction(
                        -$b - sqrt($discriminant), 2 * $a
                    ))->__toString();
                } else {
                    $rational = (new Fraction(-$b, 2 * $a))->__toString();
                    $irrational = (new Fraction(new Radical($discriminant), 2 * $a))->__toString();
                    if ($rational == '0') {
                        $this->solution['x'] = $irrational;
                        $this->solution['y'] = "-{$irrational}";
                    } else {
                        $this->solution['x'] = "{$rational}+$irrational";
                        $this->solution['y'] = "{$rational}-$irrational";
                    }
                }
            } else {
                $this->note = 'Дискриминант равен 0, корень один';
                $this->solution['x'] = (new Fraction(-$b, 2 * $a))->__toString();
            }
        }
    }

    public function __toString()
    {
        return "{$this->note}\n x1 = {$this->solution['x']}, x2 = {$this->solution['y']}";
    }

}
