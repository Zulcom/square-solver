<?php
namespace SfuKruto\model;
/**
 * Класс для обработки корней
 */
class Radical
{
    /** @var int|float подкоренное выражение */
    private $radicand;
    /** @const string знак корня */
    private $ROOT_SIGN = '√';
    private $IMAGINARY_UNIT_SIGN = 'i';

    /**
     * Radical constructor.
     * @param int|float подкоренное выражение
     */
    public function __construct($radicand)
    {
        $this->radicand = $radicand;
    }

    /**
     * @param int|float $radicand
     */
    public function setRadicand($radicand)
    {
        $this->radicand = $radicand;
    }

    /**
     * @return float|int
     */
    public function getRadicand()
    {
        return $this->radicand;
    }

    public function __toString()
    {
        $toReturn = "";
        if ($this->radicand <= 0) {
            $toReturn = "{$this->IMAGINARY_UNIT_SIGN}*";
            $this->radicand *= -1;
        }
        if ($this->radicand == 0) {
            $toReturn .= "0";
        } else {
            $perfectSquare = $this->getPerfectSquare();
            if ($perfectSquare != 1) {
                $toReturn .= $perfectSquare;
            }
            $perfectSquare *= $perfectSquare;
            if ($this->radicand != $perfectSquare) {
                $toReturn .= $this->ROOT_SIGN.$this->radicand / $perfectSquare;
            }
        }

        return $toReturn;
    }

    /** Получить полный квадрат числа
     * @return int
     */
    public function getPerfectSquare()
    {
        $a = abs($this->radicand);
        $square = 1;
        $maxDiv = floor(sqrt($a));
        for ($i = 2; $i <= $maxDiv; ++$i) {
            while ($a % ($i * $i) == 0) {
                $a /= $i * $i;
                $square *= $i;
            }
            $maxDiv = floor(sqrt($a));
        }
        return $square;
    }

    /** Является ли числом
     * @return bool
     */
    public function isInteger()
    {
        $s = $this->getPerfectSquare();

        return ($this->radicand == $s * $s);
    }

}
