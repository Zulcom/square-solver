<?php

namespace SfuKruto\model;

/**
 * Класс-обработчик дробей
 */
class Fraction
{
    /** @var int|Radical числитель */
    private $numerator;
    /** @var integer знаменатель */
    private $denominator;
    /** @var bool является радикалом */
    private $isRadical;

    /**
     * Fraction constructor.
     * @param int|Radical числитель
     * @param int знаменатель
     */
    public function __construct($numerator, $denominator)
    {
        $this->isRadical = is_a($numerator, 'SfuKruto\model\Radical');
        $this->numerator = $numerator;
        $this->denominator = $denominator;
        $this->reduce();
    }

    /**
     * Сокращает дробь
     */
    private function reduce()
    {
        if (!$this->isRadical) {
            $a = abs($this->numerator);
            $b = abs($this->denominator);
        } else {
            $a = abs($this->numerator->getPerfectSquare());
            $b = abs($this->denominator);
        }
        if ($a < $b) {
            $a ^= $b;
            $b ^= $a;
            $a ^= $b;
        }
        while ($a * $b != 0) {
            $tmp = $b;
            try {
                $b = $a % $b;
            } catch (\Throwable $e) {
                $b = 0;
                break;
            }
            $a = $tmp;
        }
        /** @var int|float Наибольший общий делитель */
        $gcd = $a + $b;
        if ($this->denominator < 0) {
            $gcd *= -1;
        }
        if (!$this->isRadical) {
            $this->numerator /= $gcd;
        } else {
            $this->numerator->setRadicand($this->numerator->getRadicand() / ($gcd * $gcd));
        }
        $this->denominator /= $gcd;
    }

    public function __toString()
    {
        if ($this->denominator == 0) {
            throw new \InvalidArgumentException('Не является числом!');
        } else {
            if (!$this->isRadical) {
                $toReturn = $this->numerator;
            } else {
                $toReturn = $this->numerator->__toString();
            }
            if ($this->denominator > 1) {
                $toReturn .= "/{$this->denominator}";
            }
        }
        return $toReturn;
    }

}
