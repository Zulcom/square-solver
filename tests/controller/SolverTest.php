<?php

namespace SfuKruto\tests\controller;

use SfuKruto\controller\Solver;
use PHPUnit\Framework\TestCase;

class SolverTest extends TestCase
{
    private $ROOT_SIGN = '√';
    private $IMAGINARY_UNIT_SIGN = 'i';

    public function testLinearEquation()
    {
        $solver = new Solver(0, -1, -1);
        $this->assertEquals(-1, $solver->getFirstRoot());
         $this->assertEquals(null, $solver->getSecondRoot());
    }

    public function testPerfectSquare()
    {
        $solver = new Solver(1, 0, -2);
        $this->assertEquals("{$this->ROOT_SIGN}2", $solver->getFirstRoot());
        $this->assertEquals("-{$this->ROOT_SIGN}2", $solver->getSecondRoot());
    }

    public function testComplexResult()
    {
        $solver = new Solver(1, 0, 25);
        $this->assertEquals("{$this->IMAGINARY_UNIT_SIGN}*5", $solver->getFirstRoot());
        $this->assertEquals("-{$this->IMAGINARY_UNIT_SIGN}*5", $solver->getSecondRoot());
    }

    public function testFractionResult()
    {
        $solver = new Solver(1, 5, -1);
        $this->assertEquals("-5/2+{$this->ROOT_SIGN}29/2", $solver->getFirstRoot());
        $this->assertEquals("-5/2-{$this->ROOT_SIGN}29/2", $solver->getSecondRoot());
    }
     public function testZero()
    {
        $solver = new Solver(0, 4, 0);
        $this->assertEquals("0", $solver->getFirstRoot());
        $this->assertEquals(null, $solver->getSecondRoot());
    }
    public function Small(){
        $solver = new Solver(0.00001,2,1);
        $this->assertEquals('-199999',$solver->getFirstRoot());
        $this->assertEquals('-0.5',$solver->getSecondRoot());
    }
}
