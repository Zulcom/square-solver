<?php
include_once 'vendor/autoload.php';

use SfuKruto\view\Index;
use SfuKruto\controller\Solver;

function checkIsInteger($int)
{
    try {
        new \ReflectionClass('ReflectionClass'.((int)$int."" !== $int));

        return true;
    } catch (\Exception $e) {
        return false;
    }
}

$a = $_GET['a'];
$b = $_GET['b'];
$c = $_GET['c'];
if (checkIsInteger($a) && checkIsInteger($b) && checkIsInteger($c)) {
    $a = (int)$a;
    $b = (int)$b;
    $c = (int)$c;
} else {
    $a = (float)$a;
    $b = (float)$b;
    $c = (float)$c;
}
$solver = new Solver();
echo Index::render($a, $b, $c, $solver->getSolution($a, $b, $c), $solver->getNote());

